# Zenklub Schedule

## Table of contents

- [General info](#general-info)
- [Main technologies](#main-technologies)
- [Quality assurance](#quality-assurance)
- [API Documentation](#api-documentation)
- [Setup](#local-setup)

## General info

The project is a appoitment system, that has just some features related with
schedule of sessions:

- It's a API;
- Has basic CRUD's related with the entities (User, Schedule);
- Has a page system on the general list of the entities to not overload the response;
- Can add filters on the query of the request on the general list endpoint's;
- Has an auth system that work with jwt to return a token that is valid for the day;
- Has a coverage of many common response errors;
- Has a local log error storage;

## Main technologies

Project is created with:

- bcryptjs v2.4.3
- commitizen v4.1.2
- commitlint v9.0.1
- lint-staged v10.2.11
- express v4.17
- eslint v7.4
- husky v4.2.5
- jest 26.1
- joi 14.3.
- jwt v8.5.1
- moment v2.27
- node mocks http: v1.8.1
- nodemon v2.0.4
- pm2 v4.4.0
- sequelize v5.22.3
- sucrase v3.15
- swagger ui express v4.1.4
- youch v2.0.10


## Quality assurance

To assure the quality of the project, some measures have been taken:

- Clean code;
- Eslint & prettier monitoring, alert and fix the code pattern of javascript related files following the airbnb styleguide;
- Directory structure design to be easy to find specific components & features;
- EditorConfig configure to force a code pattern independent of the editor;
- Git-hooks and automated tools (husky, lint-staged, commitizen, commitlint to padronize commit messages and test procedures before pushing the code on the online repository to avoid sending bug code to it;
- Unit/Integration test coverage for the controllers;
- continuous integration and continuous release with github actions and heroku;

## API documentation

I used swagger to document the API endpoints, which can be accessed by:
- developer enviromnent http://localhost:3000/api-docs
- heroku https://zenklub.herokuapp.com/api-docs

## Local setup

To run this project on development mode, install it locally using yarn:

```
$ cd ~/directory-where-the-project-was-cloned/zenklub
$ yarn
```

Install the mysql on the machine or use a online database then put the following credentials on the .env file
```
DB_HOST
DB_PORT
DB_NAME
DB_USER
DB_PASS
```

Add a `API_SECRET` on the .env file too
Run the command `$ yarn populateDB` migrations and seeds to populate the database
Then run the command `$ yarn dev` and the project is set to development on the local machine

To run tests, execute the following command `$ yarn test`

The build and deploy to production is set with github actions to deploy on heroku to be managed using pm2
