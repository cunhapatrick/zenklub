module.exports = {
	apps: [{
		watch: './build',
		instances: 4,
		exec_mode: 'cluster',
		env: {
			name: 'zenklub-challenge',
			NODE_ENV: 'production',
		},
		script: 'build/index.js',
	}],
};
