import dotenv from 'dotenv';
import Server from './server';

dotenv.config({ path: 'src/config/env/.env' });
const { PORT, NODE_ENV } = process.env;

Server.listen(PORT || 3000, () => {
	console.log(`Server is running on ${NODE_ENV} environment on uri http://localhost:${PORT || 3000}`);
});
