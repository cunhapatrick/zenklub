import express from 'express';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import Youch from 'youch';
import YAML from 'yamljs';
import fs from 'fs';
import moment from 'moment';
import routes from './routes.js';

const swaggerDocs = YAML.load('src/config/swagger.yaml');

class Server {
	constructor() {
		this.express = express();
		this.isDev = process.env.NODE_ENV !== 'production';

		this.middleware();
		this.routes();
		this.exception();
	}

	middleware() {
		// middleware activation to sentry handler
		this.express.use(express.json());
		this.express.use(cors());
	}

	routes() {
		this.express.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));
		routes(this.express);
	}

	exception() {
		// eslint-disable-next-line no-unused-vars
		this.express.use(async (err, req, res, next) => {
			if (this.isDev) {
				const youch = new Youch(err);
				const now = moment.utc(new Date());
				const log = `[${now.format('HH:mm:ss')}]: ${JSON.stringify(err)}\n`;
				console.error(youch);
				fs.appendFileSync(`${__dirname}/logs/${now.format('DD-MM-YYYY')}.txt`, log, 'utf8');
				return res.status(err.code).json(await youch.toJSON());
			}
			console.error(err);
			return res.status(err.code).json(err);
		});
	}
}

export default new Server().express;
