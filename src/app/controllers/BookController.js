import moment from 'moment';
import db from '../models';
import { BadRequest, NotFound, InternalServerError } from '../../errors';

const { Schedule, Sequelize } = db;
class BookController {
	async book(req, res) {
		const {
			professionalId, avaiableDate: calendarDate, slot, clientId,
		} = req.body;

		try {
			// check if the avaiableDate is <= then today
			if (moment(calendarDate).startOf('day').isBefore(moment().startOf('day'))) {
				const auxError = { ...BadRequest, message: 'The provided date is before today' };
				throw auxError;
			}

			const initialSlot = moment.utc(`${calendarDate} ${slot}`, 'YYYY-MM-DD HH:mm');

			// check if the send hour is inside the session range
			if (initialSlot.minute() !== 0) {
				const auxError = { ...BadRequest, message: 'Invalid time' };
				throw auxError;
			}

			/*
				Join date and hour and create a array with the joined schedule date and
				30 minutes after to complete 1 hour
			*/
			const slots = [
				initialSlot.toDate(),
				initialSlot.add(30, 'minutes').toDate(),
			];

			// get the entries of the chosen slot
			const schedules = await Schedule.findAll({
				where: {
					professionalId,
					avaiableDate: {
						[Sequelize.Op.gte]: slots[0],
						[Sequelize.Op.lte]: slots[1],
					},
				},
			});

			if (schedules.length === 0) {
				const auxError = { ...NotFound, message: 'The selected schedule is not avaiable anymore!' };
				throw auxError;
			}

			// register the client session entries
			schedules.forEach(async (schedule) => {
				schedule.bookedBy = clientId;
				schedule.bookedAt = new Date();
				await schedule.save();
			});

			return res.status(200).json(schedules);
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async unbook(req, res) {
		const {
			avaiableDate: calendarDate, slot, clientId,
		} = req.query;

		const initialSlot = moment.utc(`${calendarDate} ${slot}`, 'YYYY-MM-DDTHH:mm:ss').second(0);

		try {
			if (initialSlot.minute() !== 0) {
				const auxError = { ...BadRequest, message: 'Invalid time' };
				throw auxError;
			}

			const where = {
				bookedBy: clientId,
				avaiableDate: {
					[Sequelize.Op.gte]: initialSlot.format(),
					[Sequelize.Op.lt]: initialSlot.add(35, 'minutes').format(),
				},
			};

			const schedules = await Schedule.findAll({ where });

			if (schedules.length === 0) {
				const auxError = { ...NotFound, message: 'The selected schedule is not booked anymore!' };
				throw auxError;
			}

			schedules.forEach(async (schedule) => {
				schedule.bookedBy = null;
				schedule.bookedAt = null;
				await schedule.save();
			});

			return res.status(200).json({ entriesModified: schedules });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	// search all avaiable slot's
	async avaiableSlots(req, res) {
		const {
			initialDate, endDate, professionalId, page = 1, limit = 20,
		} = req.query;

		const where = { bookedBy: null };

		try {
			if (professionalId) where.professionalId = professionalId;
			if (initialDate && endDate) {
				where.avaiableDate = {
					[Sequelize.Op.gte]: initialDate,
					[Sequelize.Op.lte]: endDate,
				};
			} else where.avaiableDate = { [Sequelize.Op.gte]: moment().format() };
			const { count, rows } = await Schedule.findAndCountAll({
				where,
				offset: parseInt(page, 10) - 1,
				limit: parseInt(limit, 10),
			});

			return res.status(200).json({ data: rows, total: count, currentPage: parseInt(page, 10) });
		} catch (error) {
			const err = { ...InternalServerError, internalError: error };
			throw err;
		}
	}
}

export default new BookController();
