import db from '../models';
import { BadRequest, InternalServerError } from '../../errors';

class SessionController {
	async store(req, res) {
		const { email, password } = req.body;

		try {
			const user = await db.User.findOne({ where: { email } });

			if (!user) {
				const auxError = { ...BadRequest, message: 'Invalid email' };
				throw auxError;
			}

			// method created on user model
			if (!(await user.checkPassword(password))) {
				const auxError = { ...BadRequest, message: 'Invalid password' };
				throw auxError;
			}

			const result = user.get({ plain: true });
			delete result.passwordHash;

			return res.status(200).json({ ...result, token: user.generateToken(user) });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}
}

export default new SessionController();
