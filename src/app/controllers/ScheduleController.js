import moment from 'moment';
import db from '../models';
import {
	BadRequest, UnprocessableEntity, InternalServerError, NotFound,
} from '../../errors';

const { Schedule, Sequelize } = db;
class ScheduleController {
	async index(req, res) {
		const {
			avaiableDate, professionalId, bookedBy, page = 1, limit = 20,
		} = req.query;
		const where = {};

		if (avaiableDate) {
			where.avaiableDate = {
				[Sequelize.Op.gte]: moment.utc(avaiableDate).startOf('day').format(),
				[Sequelize.Op.lt]: moment.utc(avaiableDate).endOf('day').format(),
			};
		}
		if (professionalId) where.professionalId = professionalId;
		if (bookedBy) where.bookedBy = bookedBy;

		try {
			const { count, rows } = await Schedule.findAndCountAll({
				where,
				offset: parseInt(page, 10) - 1,
				limit: parseInt(limit, 10),
				order: [
					['professionalId', 'ASC'],
					['avaiableDate', 'ASC'],
				],
			});

			return res.status(200).json({ data: rows, total: count, currentPage: parseInt(page, 10) });
		} catch (error) {
			const err = { ...InternalServerError, internalError: error };
			throw err;
		}
	}

	async show(req, res) {
		const { id } = req.params;
		try {
			if (!parseInt(id, 10)) {
				const auxError = { ...UnprocessableEntity, message: 'Invalid id type' };
				throw auxError;
			}

			const schedule = await Schedule.findByPk(id);
			const result = schedule.get({ plain: true });

			return res.json(result);
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async store(req, res) {
		const { avaiableDate: calendarDate, slots, professionalId } = req.body;

		try {
			// check if the avaiableDate is <= then today
			if (moment(calendarDate).startOf('day').isBefore(moment().startOf('day'))) {
				const auxError = { ...BadRequest, message: 'The provided date is before today' };
				throw auxError;
			}

			// convert the array time type to date and sort it by time diff between them
			const momentTime = slots
				.map((slot) => moment.utc(`${calendarDate} ${slot}`))
				.sort((a, b) => a.diff(b));

			// Check if the minutes from the provided array time is 00 or 30 and check if
			// the provided array time will overlap another
			const invalidTime = momentTime.some((time) => time.minute() !== 0)
		&& momentTime.some((time, i) => {
			if (i !== 0) return time.diff(momentTime[i - 1], 'hours') >= 1;
			return true;
		});

			if (invalidTime) {
				const auxError = { ...BadRequest, message: 'Invalid time' };
				throw auxError;
			}

			const arrayDate = [];

			momentTime.forEach((slot) => {
				arrayDate.push(slot.toDate());
				arrayDate.push(slot.add(30, 'minutes').toDate());
			});

			arrayDate.forEach((date) => Schedule.create({ avaiableDate: date, professionalId }));
			return res.status(201).json({ message: 'success schedule' });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async update(req, res) {
		const { professionalId, avaiableDate: calendarDate, slot } = req.body;
		const { id } = req.params;

		try {
			if (!parseInt(id, 10)) {
				const auxError = { ...UnprocessableEntity, message: 'Invalid id type' };
				throw auxError;
			}

			const where = { professionalId: id };
			const initialDate = moment.utc(`${calendarDate} ${slot}`);

			if (initialDate.minute() !== 0) {
				const auxError = { ...UnprocessableEntity, message: 'Invalid time' };
				throw auxError;
			}

			where.avaiableDate = {
				[Sequelize.Op.gte]: initialDate.format(),
				[Sequelize.Op.lte]: initialDate.add(35, 'minutes').format(),
			};

			const schedules = await Schedule.findAll({ where });

			if (schedules.length === 0) {
				const auxErr = {
					...NotFound,
					message: 'The is no entry avaiable with the provided data or professional',
				};
				throw auxErr;
			}

			schedules.forEach((schedule) => {
				schedule.professionalId = professionalId;
				schedule.updatedAt = new Date();
				schedule.save();
			});

			return res.status(200).json(schedules);
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async destroy(req, res) {
		const { avaiableDate: calendarDate, slot } = req.body;
		const { id } = req.params;

		try {
			if (!parseInt(id, 10)) {
				const auxError = { ...UnprocessableEntity, message: 'Invalid id type' };
				throw auxError;
			}

			const where = { professionalId: id };
			const initialDate = moment.utc(`${calendarDate} ${slot}`);

			if (initialDate.minute() !== 0) {
				const auxError = { ...UnprocessableEntity, message: 'Invalid time' };
				throw auxError;
			}

			where.avaiableDate = {
				[Sequelize.Op.gte]: initialDate.format(),
				[Sequelize.Op.lt]: initialDate.add(35, 'minutes').format(),
			};

			const schedule = await Schedule.destroy({ where });

			if (schedule === 1) return res.status(200).json({ message: 'ok' });
			return res.status(200).json({ message: 'No entry was deleted' });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}
}

export default new ScheduleController();
