import db from '../models';
import { NotFound, UnprocessableEntity, InternalServerError } from '../../errors';

const { User } = db;
class UserController {
	async index(req, res) {
		const {
			name, email, provider, page = 1, limit = 20,
		} = req.query;
		const where = {};

		if (name) where.name = name;
		if (email) where.email = email;
		if (provider) where.provider = provider;

		try {
			const { count, rows } = await User.findAndCountAll({
				where,
				offset: parseInt(page, 10) - 1,
				limit: parseInt(limit, 10),
				attributes: ['id', 'name', 'email', 'provider', 'createdAt', 'updatedAt'],
			});
			return res.status(200).json({ data: rows, total: count, currentPage: parseInt(page, 10) });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async show(req, res) {
		const { id } = req.params;

		try {
			if (!parseInt(id, 10)) throw UnprocessableEntity;

			const user = await User.findByPk(id, {
				attributes: ['id', 'name', 'email', 'provider', 'createdAt', 'updatedAt'],
			});
			if (!user) {
				const auxError = { ...NotFound, message: 'No user found with the provided id' };
				throw auxError;
			}

			const result = user.get({ plain: true });
			delete result.passworHash;

			return res.json(result);
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async store(req, res) {
		const {
			name, email, password, provider,
		} = req.body;

		try {
			const user = await User.create({
				name, email, password, provider,
			});

			const returnUser = { ...user.get({ plain: true }) };
			delete returnUser.password;
			delete returnUser.passwordHash;

			return res.status(201).json(returnUser);
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async update(req, res) {
		const {
			name, email, password, provider,
		} = req.body;
		const { id } = req.params;

		console.log(!parseInt(id, 10));

		try {
			if (!parseInt(id, 10)) throw UnprocessableEntity;
			const obj = {};
			const user = await User.findByPk(id);

			if (!user) {
				const auxError = { ...NotFound, message: 'No user found with the provided id' };
				throw auxError;
			}

			if (name) obj.name = name;
			if (email) obj.email = email;
			if (password) obj.password = password;
			if (provider) obj.provider = provider;

			if (name || email || password) {
				obj.updatedAt = new Date();
				await user.update(obj, { fields: ['id', 'name', 'email', 'provider', 'createdAt', 'updatedAt'] });
				const result = { ...user.get({ plain: true }) };
				delete result.passwordHash;
				delete result.password;
				return res.status(200).json(result);
			}
			return res.status(200).json({ msg: 'No field was changed' });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}

	async destroy(req, res) {
		const { id } = req.params;
		try {
			if (!parseInt(id, 10)) throw UnprocessableEntity;
			const user = await User.destroy({ where: { id } });
			if (user === 1) return res.status(200).json({ message: 'ok' });
			return res.status(200).json({ message: 'No user was deleted' });
		} catch (error) {
			if (error.code) throw error;
			else {
				const err = { ...InternalServerError, internalError: error };
				throw err;
			}
		}
	}
}

export default new UserController();
