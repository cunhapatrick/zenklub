import Joi from 'joi';
import { UnprocessableEntity } from '../../errors';

export default (schema, parameter) => (req, res, next) => {
	const { error } = Joi.validate(req[parameter], schema);
	const valid = error === null;

	if (valid) next();
	else {
		const { details } = error;
		const message = details.map((i) => i.message).join(',');
		const err = { ...UnprocessableEntity, message };
		next(err);
	}
};
