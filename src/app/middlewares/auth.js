import jwt from 'jsonwebtoken';
import { promisify } from 'util';
import { Unauthorized, InternalServerError } from '../../errors';
import db from '../models';

export default async (req, res, next) => {
	const { headers } = req;
	const authHeader = headers.authorization;

	try {
		if (!authHeader) throw Unauthorized;

		const [, token] = authHeader.split(' ');

		if (!token) throw Unauthorized;

		const decoded = await promisify(jwt.verify)(token, process.env.API_SECRET);
		const user = await db.User.findByPk(decoded.id);

		if (!user) throw Unauthorized;

		req.userId = decoded.id;
		next();
	} catch (error) {
		if (error.name === 'TokenExpiredError') {
			error.message = 'Access token expired';
			return next(error);
		}
		if (error.code === 401 || error.name === 'JsonWebTokenError') return next(Unauthorized);
		next(InternalServerError);
	}
};
