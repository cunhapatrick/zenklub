import moment from 'moment';
import bcryptjs from 'bcryptjs';
import dotenv from 'dotenv';
import { sign } from 'jsonwebtoken';

dotenv.config({ path: 'src/config/env/.env' });

export default (sequelize, DataTypes) => {
	const User = sequelize.define('User', {
		name: {
			type: DataTypes.STRING,
			required: true,
		},
		email: {
			type: DataTypes.STRING,
			required: true,
		},
		password: {
			type: DataTypes.VIRTUAL,
			required: true,
		},
		passwordHash: DataTypes.STRING,
		provider: {
			type: DataTypes.INTEGER,
			required: true,
		},
		createdAt: {
			type: DataTypes.DATE,
			get() {
				return moment(this.getDataValue('createdAt')).format('DD/MM/YYYY HH:mm:ss');
			},
		},
		updatedAt: {
			type: DataTypes.DATE,
			get() {
				return moment(this.getDataValue('updatedAt')).format('DD/MM/YYYY HH:mm:ss');
			},
		},
	});

	User.addHook('beforeCreate', (user) => {
		user.createdAt = new Date();
		user.updatedAt = new Date();
		user.passwordHash = bcryptjs.hashSync(user.password, 10);
	});

	User.addHook('beforeUpdate', (user) => {
		if (user.password) user.passwordHash = bcryptjs.hashSync(user.password, 10);
		user.updatedAt = new Date();
	});

	User.prototype.checkPassword = function checkPassword(password) {
		return bcryptjs.compareSync(password, this.passwordHash);
	};

	User.prototype.generateToken = function generateToken({ id }) {
		return sign({ id }, process.env.API_SECRET, { expiresIn: 86400 });
	};

	return User;
};
