export default (sequelize, DataTypes) => {
	const Schedule = sequelize.define('Schedule', {
		avaiableDate: {
			type: DataTypes.DATE,
			required: true,
		},
		createdAt: DataTypes.DATE,
		updatedAt: DataTypes.DATE,
		bookedAt: {
			type: DataTypes.DATE,
			required: false,
		},
	});

	Schedule.addHook('beforeCreate', (schedule) => {
		schedule.createdAt = new Date();
		schedule.updatedAt = new Date();
	});

	Schedule.associate = (models) => {
		Schedule.belongsTo(models.User, {
			foreignKey: {
				name: 'bookedBy',
				allowNull: true,
			},
		});
		Schedule.belongsTo(models.User, {
			foreignKey: {
				name: 'professionalId',
				allowNull: false,
			},
		});
	};

	return Schedule;
};
