import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import dotenv from 'dotenv';
import config from '../../config/database.js';

dotenv.config({ path: 'src/config/env/.env' });

const basename = path.basename(__filename);
const db = {};
const env = process.env.NODE_ENV;

const sequelize = new Sequelize(
	config[env].database,
	config[env].username,
	config[env].password,
	config[env],
);

try {
	sequelize.authenticate().then(() => console.log('Connection stabilished!'));
} catch (err) {
	console.error('Unable to connect to the database:', err);
}

fs.readdirSync(__dirname)
	.filter((file) => (
		file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'
	))
	.forEach((file) => {
		const model = sequelize.import(path.join(__dirname, file));
		db[model.name] = model;
	});

Object.keys(db).forEach((modelName) => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
