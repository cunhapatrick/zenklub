import Joi from 'joi';

export default {
	email: Joi.string().email().required(),
	password: Joi.string().min(6).required(),
};
