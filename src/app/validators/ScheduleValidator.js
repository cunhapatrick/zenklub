import Joi from 'joi';

export const BodyValidator = {
	avaiableDate: Joi.date().required(),
	slots: Joi.array().items(Joi.string().regex(/([01]?[0-9]|2[0-3]):([0-5][0-9])/).required()),
	professionalId: Joi.number().required(),
};

export const BodyUpdateValidator = {
	avaiableDate: Joi.date().required(),
	slot: Joi.string().regex(/([01]?[0-9]|2[0-3]):([0-5][0-9])/).required(),
	professionalId: Joi.number().required(),
};

export const QueryValidator = {
	avaiableDate: Joi.date(),
	professionalId: Joi.number(),
	filledBy: Joi.number(),
	page: Joi.number(),
	limit: Joi.number(),
};
