import Joi from 'joi';

export default {
	name: Joi.string().required(),
	email: Joi.string().email().required(),
	password: Joi.string().required().min(6),
	provider: Joi.number().min(0).max(1).required(),
};
