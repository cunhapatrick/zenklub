const bcryptjs = require('bcryptjs');

module.exports = {
	up: (queryInterface) => queryInterface.bulkInsert('Users', [{
		name: 'TestClient',
		email: 'client@email.com',
		passwordHash: bcryptjs.hashSync('abc123', 10),
		provider: 0,
	}, {
		name: 'TestProfessional',
		email: 'professional@email.com',
		passwordHash: bcryptjs.hashSync('abc123', 10),
		provider: 1,
	}, {
		name: 'TestClient2',
		email: 'client2@email.com',
		passwordHash: bcryptjs.hashSync('abc123', 10),
		provider: 0,
	}, {
		name: 'TestProfessional',
		email: 'professional2@email.com',
		passwordHash: bcryptjs.hashSync('abc123', 10),
		provider: 1,
	}]),
	down: (queryInterface) => queryInterface.bulkDelete('Users', null, {}),
};
