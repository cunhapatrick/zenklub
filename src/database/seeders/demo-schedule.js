const moment = require('moment');

const avaiableDate = moment();

module.exports = {
	up: (queryInterface) => queryInterface.bulkInsert('Schedules', [{
		avaiableDate: avaiableDate.hour(9).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: null,
	}, {
		avaiableDate: avaiableDate.hour(9).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: null,
	}, {
		avaiableDate: avaiableDate.hour(10).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(10).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(11).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: null,
	}, {
		avaiableDate: avaiableDate.hour(11).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: null,
	}, {
		avaiableDate: avaiableDate.hour(8).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(8).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(12).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(12).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(13).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(13).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(14).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(14).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(15).minute(0).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}, {
		avaiableDate: avaiableDate.hour(15).minute(30).format('YYYY-MM-DDTHH:mm:ss'),
		professionalId: 2,
		bookedBy: 1,
		bookedAt: new Date(),
	}]),
	down: (queryInterface) => queryInterface.bulkDelete('Schedules', null, {}),
};
