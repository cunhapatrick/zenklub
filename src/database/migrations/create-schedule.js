module.exports = {
	up: (queryInterface, Sequelize) => queryInterface.createTable('Schedules', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER,
		},
		avaiableDate: {
			type: Sequelize.DATE,
			allowNull: false,
		},
		professionalId: {
			type: Sequelize.INTEGER,
			references: {
				model: 'Users',
				key: 'id',
			},
			allowNull: false,
		},
		bookedBy: {
			type: Sequelize.INTEGER,
			references: {
				model: 'Users',
				key: 'id',
			},
			allowNull: true,
		},
		createdAt: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.fn('NOW'),
		},
		updatedAt: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.fn('NOW'),
		},
		bookedAt: {
			type: Sequelize.DATE,
			defaultValue: null,
		},
	}),
	down: (queryInterface) => queryInterface.dropTable('Schedules'),
};
