module.exports = {
	up: (queryInterface, Sequelize) => queryInterface.createTable('Users', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: Sequelize.INTEGER,
		},
		name: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		passwordHash: {
			type: Sequelize.STRING,
			allowNull: false,
		},
		provider: { // 0 -> client / 1 -> professional
			type: Sequelize.INTEGER,
			allowNull: false,
		},
		createdAt: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.fn('NOW'),
		},
		updatedAt: {
			type: Sequelize.DATE,
			defaultValue: Sequelize.fn('NOW'),
		},
	}),
	down: (queryInterface) => queryInterface.dropTable('Users'),
};
