/*
	The server cannot or will not process the request due to an apparent client error
*/
export const BadRequest = {
	name: 'BadRequest',
	code: 400,
	message: '',
};

/*
	Similar to 403 Forbidden, but specifically for use when authentication is
	required and has failed or has not yet been provided
*/
export const Unauthorized = {
	name: 'Unauthorized',
	code: 401,
	message: 'Access token is missing or invalid',
};

/*
	The requested resource could not be found but may be available in the future.
	Subsequent requests by the client are permissible.
*/
export const NotFound = {
	name: 'NotFound',
	code: 404,
	message: 'The specific resource was not found',
};

/*
 A request method is not supported for the requested resource;
 For example, a GET request on a form that requires data to be presented via POST,
 or a PUT request on a read-only resource.
*/

export const MethodNotAllowed = {
	code: 405,
	name: 'MethodNotAllowed',
};

/*
	The request was well-formed but was unable to be followed due to semantic errors.
*/

export const UnprocessableEntity = {
	code: 422,
	name: 'UnprocessableEntity',
};

/*
	A generic error message, given when an unexpected condition was encountered
	and no more specific message is suitable.
*/

export const InternalServerError = {
	code: 500,
	name: 'InternalServerError',
};
