import httpMock from 'node-mocks-http';
import events from 'events';
import UserController from '../../app/controllers/UserController';

/* eslint-disable */

describe('UserController test', () => {
	let reqIndex;
	let reqShow;
	let reqStore;
	let reqUpdate;
	let reqDestroy;
	let res;
	let id;

	const body = {
		name: 'Teste',
		email: 'teste@xpto.com.br',
		password: '123456789',
		provider: 0,
	};

	beforeEach(() => {
		reqIndex = httpMock.createRequest({
			method: 'GET',
			url: '/v1/users',
		});

		reqShow = httpMock.createRequest({
			method: 'GET',
			url: '/v1/users',
			params: { id: 1 },
		});

		reqStore = httpMock.createRequest({
			method: 'POST',
			url: '/v1/users',
			body,
		});

		reqUpdate = httpMock.createRequest({
			method: 'PUT',
			url: '/v1/users',
			body,
			params: { id: 3 }
		});

		reqDestroy = httpMock.createRequest({
			method: 'DELETE',
			url: '/v1/users',
			params: { id },
		});

		res = httpMock.createResponse({
			eventEmitter: events.EventEmitter,
		});
	});

	describe('List of users on UserController index function', () => {
		it('Should return an object with more then 0 entries', async () => {
			const result = await UserController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBeGreaterThan(0);
		});

		it('Should return an object with one entry and all filters should work', async () => {
			reqIndex.query = {
				name: 'TestClient',
				email: 'client@email.com',
				provider: 0,
				page: 1,
				limit: 10,
			};

		const result = await UserController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBe(1);
		});

		it('Should return an empty object', async () => {
			reqIndex.query = { name: 'Xpto' };
			const result = await UserController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBe(0);
		});
	});

	describe('Show a user entry with a provided id', () => {
		it('Should return one object user entry', async () => {
			const result = await UserController.show(reqShow, res);
			expect(JSON.parse(result._getData()).name).toBe('TestClient');
		});
	});

	describe('Register a user', () => {
		it('Should store a user entry on the system', async () => {
			const result = await UserController.store(reqStore, res);
			const user = JSON.parse(result._getData());
			expect(user.name).toBe('Teste');
			id = user.id;
		});
	});

	describe('Update user entry', () => {
		it('Should update a user entry', async () => {
			reqUpdate.body = {
				name: 'AutoTest',
				email: 'autoTest@xpto.com',
				provider: 1,
			};

			const result = await UserController.update(reqUpdate, res);
			expect(JSON.parse(result._getData()).name).toBe('AutoTest');
		});
	});

	describe('Remove the user entry', () => {
		it('Should remove the user entry with the provided id', async () => {
			const result = await UserController.destroy(reqDestroy, res);
			expect(JSON.parse(result._getData()).message).toBe('ok');
		});
	});
});
