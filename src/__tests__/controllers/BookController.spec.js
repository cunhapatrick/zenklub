import moment from 'moment';
import httpMock from 'node-mocks-http';
import events from 'events';
import BookController from '../../app/controllers/BookController';

/* eslint-disable */
describe('BookController test', () => {
	let reqAvaiable;
	let reqBook;
	let reqUnbook;
	let res;
	const now = moment().format('YYYY-MM-DD');

	const body = {
		avaiableDate: now,
		slot: '14:00',
		professionalId: 2,
		clientId: 1,
	};

	beforeEach(() => {
		reqAvaiable = httpMock.createRequest({
			method: 'GET',
			url: '/v1/books',
		});

		reqBook = httpMock.createRequest({
			method: 'POST',
			url: '/v1/books',
			body,
		});

		reqUnbook = httpMock.createRequest({
			method: 'DELETE',
			url: '/v1/books',
			query: { ...body, slot: '15:00' },
		});

		res = httpMock.createResponse({
			eventEmitter: events.EventEmitter,
		});
	});

	describe('List of avaiable dates', () => {
		it('Should return more then 0 entries', async () => {
			const result = await BookController.avaiableSlots(reqAvaiable, res);
			expect(JSON.parse(result._getData()).total).toBeGreaterThan(0);
		});

		it('Should return an object with one entry and all filters should work', async () => {
			reqAvaiable.query = {
				initialDate: moment(now).startOf('day'),
				endDate: moment(now).add(1, 'days').endOf('day'),
				professionalId: 2,
				page: 1,
				limit: 10,
			};

			const result = await BookController.avaiableSlots(reqAvaiable, res);
			expect(JSON.parse(result._getData()).total).toBeGreaterThan(0);
		});

		it('Should return an empty object', async () => {
			reqAvaiable.query = { professionalId: 999999 };
			const result = await BookController.avaiableSlots(reqAvaiable, res);
			expect(JSON.parse(result._getData()).total).toBe(0);
		});
	});

	describe('Book a session', () => {
		it('Should book session entries on the system', async () => {
			const result = await BookController.book(reqBook, res);
			expect(JSON.parse(result._getData()).length).toBeGreaterThan(0);
		});

		it('Should throw a exception when send a invalid time', async () => {
			reqBook.body.slot = '08:45';
			try {
				await BookController.book(reqBook, res);
			} catch (err) {
				expect(err.name).toBe('BadRequest');
				expect(err.message).toBe('Invalid time');
			}
		});

		it('Should throw a exception when send a session time that is not avaiable', async () => {
			reqBook.body.slot = '17:00';
			try {
				await BookController.book(reqBook, res);
			} catch (err) {
				expect(err.name).toBe('NotFound');
				expect(err.message).toBe('The selected schedule is not avaiable anymore!');
			}
		});
	});

	describe('Unbook the session entries', () => {
		it('Should unbook the session entries with the provided data', async () => {
			const result = await BookController.unbook(reqUnbook, res);
			expect(JSON.parse(result._getData()).entriesModified).toBeDefined();
		});

		it('Should throw a exception when send a invalid time', async () => {
			reqUnbook.query.slot = '08:45';
			try {
				await BookController.unbook(reqUnbook, res);
			} catch (err) {
				expect(err.name).toBe('BadRequest');
				expect(err.message).toBe('Invalid time');
			}
		});

		it('Should throw a exception when send a session time that is not booked', async () => {
			reqBook.body.slot = '17:00';
			try {
				await BookController.unbook(reqUnbook, res);
			} catch (err) {
				expect(err.name).toBe('NotFound');
				expect(err.message).toBe('The selected schedule is not booked anymore!');
			}
		});
	});
});
