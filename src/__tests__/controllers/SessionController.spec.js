import httpMock from 'node-mocks-http';
import events from 'events';
import SessionController from '../../app/controllers/SessionController';

/* eslint-disable */

describe('SessionController test', () => {
	let request;
	let res;

	const body = {
		email: 'client@email.com',
		password: 'abc123',
	};

	beforeEach(() => {
		request = httpMock.createRequest({
			method: 'POST',
			url: '/v1/sessions',
			body,
		});

		res = httpMock.createResponse({
			eventEmitter: events.EventEmitter,
		});
	});

	it('Should return an object with user info and a token', async () => {
		const result = await SessionController.store(request, res);
		expect(JSON.parse(result._getData()).token).toBeDefined();
		expect(JSON.parse(result._getData()).email).toBe('client@email.com');
	});

	it('Should return an error object informing the email is invalid', async () => {
		request.body.email = "teste@gmail.com";
		try {
			await SessionController.store(request, res);
		} catch (err) {
			expect(err.name).toBe('BadRequest');
			expect(err.message).toBe('Invalid email');
		}

	});

	it('Should return an error object informing the email is invalid', async () => {
		request.body = { email: 'client@email.com', password: "teste@gmail.com" };
		try {
			await SessionController.store(request, res);
		} catch (err) {
			expect(err.name).toBe('BadRequest');
			expect(err.message).toBe('Invalid password');
		}
	});
});
