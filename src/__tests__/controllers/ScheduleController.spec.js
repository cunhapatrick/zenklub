import moment from 'moment';
import httpMock from 'node-mocks-http';
import events from 'events';
import ScheduleController from '../../app/controllers/ScheduleController';

/* eslint-disable */

describe('ScheduleController test', () => {
	let reqIndex;
	let reqShow;
	let reqStore;
	let reqUpdate;
	let reqDestroy;
	let res;
	const now = moment().format('YYYY-MM-DD');

	const body = {
		avaiableDate: now,
		slots: ['08:00', '09:00', '10:00', '11:00', '12:00'],
		professionalId: 4,
	};

	beforeEach(() => {
		reqIndex = httpMock.createRequest({
			method: 'GET',
			url: '/v1/sessions',
		});

		reqShow = httpMock.createRequest({
			method: 'GET',
			url: '/v1/sessions',
			params: { id: 1 },
		});

		reqStore = httpMock.createRequest({
			method: 'POST',
			url: '/v1/sessions',
			body,
		});

		reqUpdate = httpMock.createRequest({
			method: 'PUT',
			url: '/v1/sessions',
			body,
			params: { id: 2 }
		});

		reqDestroy = httpMock.createRequest({
			method: 'DELETE',
			url: '/v1/sessions',
			params: { id: 4 },
		});

		res = httpMock.createResponse({
			eventEmitter: events.EventEmitter,
		});
	});

	describe('List of schedule entries', () => {
		it('Should return an object with more then 0 entries', async () => {
			const result = await ScheduleController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBeGreaterThan(0);
		});

		it('Should return an object with one entry and all filters should work', async () => {
			reqIndex.query = {
				avaiableDate: now,
				professionalId: 2,
				page: 1,
				limit: 10,
			};

			const result = await ScheduleController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBeGreaterThan(5);
		});

		it('Should return an empty object', async () => {
			reqIndex.query = { professionalId: 999999 };
			const result = await ScheduleController.index(reqIndex, res);
			expect(JSON.parse(result._getData()).total).toBe(0);
		});
	});

	describe('Show a schedule entry with a provided id', () => {
		it('Should return one entry', async () => {
			const result = await ScheduleController.show(reqShow, res);
			expect(JSON.parse(result._getData()).professionalId).toBe(2);
		});
	});

	describe('Register a session avaiability', () => {
		it('Should store session entries on the system', async () => {
			const result = await ScheduleController.store(reqStore, res);
			expect(JSON.parse(result._getData()).message).toBe('success schedule');
		});
	});

	describe('Update schedule session entries', () => {
		it('Should update the session entries of the provided data', async () => {
			reqUpdate.body = {
				avaiableDate: now,
				professionalId: 5,
				slot: '11:00',
			};

			const result = await ScheduleController.update(reqUpdate, res);
			expect(JSON.parse(result._getData()).length).toBeGreaterThan(1);
		});
	});

	describe('Remove the session entries', () => {
		it('Should remove the session entries with the provided data', async () => {
			reqDestroy.body = { professionalId: 4, slot: '12:00', avaiableDate: now };
			const result = await ScheduleController.destroy(reqDestroy, res);
			expect(JSON.parse(result._getData()).message).toBeDefined();
		});
	});
});
