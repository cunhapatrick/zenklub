import handler from 'express-async-handler';
import authMiddleware from './app/middlewares/auth.js';
import validateMiddleware from './app/middlewares/validation.js';
import SessionController from './app/controllers/SessionController.js';
import SessionValidator from './app/validators/SessionValidator.js';
import UserController from './app/controllers/UserController.js';
import UserValidator from './app/validators/UserValidator.js';
import ScheduleController from './app/controllers/ScheduleController.js';
import * as ScheduleValidator from './app/validators/ScheduleValidator.js';
import BookController from './app/controllers/BookController.js';
import * as BookValidator from './app/validators/BookValidator';
import { NotFound, MethodNotAllowed } from './errors';

export default (app) => {
	app.route('/v1/users')
		.get(handler(UserController.index))
		.post(validateMiddleware(UserValidator, 'body'), handler(UserController.store));

	app.route('/v1/users/:id')
		.get(handler(UserController.show))
		.put(validateMiddleware(UserValidator, 'body'), handler(UserController.update))
		.delete(handler(UserController.destroy));

	// Session endpoint to generate the token valid for 1 day
	app.route('/v1/sessions')
		.post(validateMiddleware(SessionValidator, 'body'), handler(SessionController.store));

	// CRUD route schedule entity
	app.route('/v1/schedules')
		.get([
			authMiddleware,
			validateMiddleware(ScheduleValidator.QueryValidator, 'query'),
		], handler(ScheduleController.index))
		.post([
			authMiddleware,
			validateMiddleware(ScheduleValidator.BodyValidator, 'body'),
		], handler(ScheduleController.store));

	app.route('/v1/schedules/:id')
		.get(authMiddleware, handler(ScheduleController.show))
		.put([
			authMiddleware,
			validateMiddleware(ScheduleValidator.BodyUpdateValidator, 'body'),
		], handler(ScheduleController.update))
		.delete([
			authMiddleware,
			validateMiddleware(ScheduleValidator.BodyUpdateValidator, 'body'),
		], handler(ScheduleController.destroy));

	// Endpoints to book and check calendar avaiability
	app.route('/v1/books')
		.put([
			authMiddleware,
			validateMiddleware(BookValidator.BodyValidator, 'body'),
		], handler(BookController.book))
		.delete([
			authMiddleware,
			validateMiddleware(BookValidator.QueryValidator, 'query'),
		], handler(BookController.unbook))
		.get([
			authMiddleware,
			validateMiddleware(BookValidator.AvaiabilityValidator, 'query'),
		], handler(BookController.avaiableSlots));

	app.route('*')
		.get(() => { throw NotFound; })
		.post(() => { throw NotFound; })
		.put(() => { throw NotFound; })
		.delete(() => { throw NotFound; })
		.patch(() => { throw MethodNotAllowed; });
};
