const dotenv = require('dotenv');

dotenv.config({ path: 'src/config/env/.env' });

const {
	DB_USER,
	DB_PASS,
	DB_NAME,
	DB_HOST,
	DB_TEST_USER,
	DB_TEST_PASS,
	DB_TEST_NAME,
	DB_TEST_HOST,
} = process.env;

module.exports = {
	development: {
		dialect: 'mysql',
		host: DB_HOST,
		username: DB_USER,
		password: DB_PASS,
		database: DB_NAME,
		define: {
			timestamps: false,
		},
	},
	test: {
		dialect: 'mysql',
		host: DB_TEST_HOST,
		username: DB_TEST_USER,
		password: DB_TEST_PASS,
		database: DB_TEST_NAME,
		define: {
			timestamps: false,
		},
		logging: false,
	},
	production: {
		dialect: 'mysql',
		host: DB_HOST,
		username: DB_USER,
		password: DB_PASS,
		database: DB_NAME,
		define: {
			timestamps: false,
		},
	},
};
